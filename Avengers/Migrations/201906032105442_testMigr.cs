namespace Avengers.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testMigr : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Test",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),

                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.Actors", "Test", c => c.String());
            CreateIndex("dbo.Actors", "Test");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Actors", "Test");
            DropTable("dbo.Test");
            DropIndex("dbo.Actors", "Test");
        }
    }
}

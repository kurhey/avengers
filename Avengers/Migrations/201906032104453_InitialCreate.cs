namespace Avengers.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 8000, unicode: false),
                        Surname = c.String(),
                        Birthday = c.DateTime(nullable: false, storeType: "date"),
                        Salary = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FilmActors",
                c => new
                    {
                        FilmId = c.Int(nullable: false),
                        ActorId = c.Int(nullable: false),
                        Rating = c.Decimal(nullable: false, precision: 2, scale: 2),
                    })
                .PrimaryKey(t => new { t.FilmId, t.ActorId })
                .ForeignKey("dbo.Actors", t => t.ActorId, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.FilmId, cascadeDelete: true)
                .Index(t => t.FilmId)
                .Index(t => t.ActorId);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Genre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.battles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Place = c.String(),
                        FilmId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Films", t => t.FilmId)
                .Index(t => t.FilmId);
            
            CreateTable(
                "dbo.Heroes",
                c => new
                    {
                        UniqNumber = c.Int(nullable: false),
                        HeroName = c.String(nullable: false, maxLength: 128),
                        Side = c.String(),
                        Avenger = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.UniqNumber, t.HeroName });
            
            CreateTable(
                "dbo.Inventions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        HeroId = c.Int(),
                        Hero_UniqNumber = c.Int(),
                        Hero_Name = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Heroes", t => new { t.Hero_UniqNumber, t.Hero_Name })
                .Index(t => new { t.Hero_UniqNumber, t.Hero_Name });
            
            CreateTable(
                "dbo.Comics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Part = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FilmActors1",
                c => new
                    {
                        Film_Id = c.Int(nullable: false),
                        Actor_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Film_Id, t.Actor_Id })
                .ForeignKey("dbo.Films", t => t.Film_Id, cascadeDelete: true)
                .ForeignKey("dbo.Actors", t => t.Actor_Id, cascadeDelete: true)
                .Index(t => t.Film_Id)
                .Index(t => t.Actor_Id);
            
            CreateTable(
                "dbo.HeroActors",
                c => new
                    {
                        Hero_UniqNumber = c.Int(nullable: false),
                        Hero_Name = c.String(nullable: false, maxLength: 128),
                        Actor_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Hero_UniqNumber, t.Hero_Name, t.Actor_Id })
                .ForeignKey("dbo.Heroes", t => new { t.Hero_UniqNumber, t.Hero_Name }, cascadeDelete: true)
                .ForeignKey("dbo.Actors", t => t.Actor_Id, cascadeDelete: true)
                .Index(t => new { t.Hero_UniqNumber, t.Hero_Name })
                .Index(t => t.Actor_Id);
            
            CreateTable(
                "dbo.HeroBattles",
                c => new
                    {
                        Hero_UniqNumber = c.Int(nullable: false),
                        Hero_Name = c.String(nullable: false, maxLength: 128),
                        Battle_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Hero_UniqNumber, t.Hero_Name, t.Battle_Id })
                .ForeignKey("dbo.Heroes", t => new { t.Hero_UniqNumber, t.Hero_Name }, cascadeDelete: true)
                .ForeignKey("dbo.battles", t => t.Battle_Id, cascadeDelete: true)
                .Index(t => new { t.Hero_UniqNumber, t.Hero_Name })
                .Index(t => t.Battle_Id);
            
            CreateTable(
                "dbo.HeroFilms",
                c => new
                    {
                        Hero_UniqNumber = c.Int(nullable: false),
                        Hero_Name = c.String(nullable: false, maxLength: 128),
                        Film_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Hero_UniqNumber, t.Hero_Name, t.Film_Id })
                .ForeignKey("dbo.Heroes", t => new { t.Hero_UniqNumber, t.Hero_Name }, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.Film_Id, cascadeDelete: true)
                .Index(t => new { t.Hero_UniqNumber, t.Hero_Name })
                .Index(t => t.Film_Id);
            
            CreateTable(
                "dbo.ComicsInfo",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Publisher = c.String(),
                        Rating = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Comics", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ComicsInfo", "Id", "dbo.Comics");
            DropForeignKey("dbo.FilmActors", "FilmId", "dbo.Films");
            DropForeignKey("dbo.Inventions", new[] { "Hero_UniqNumber", "Hero_Name" }, "dbo.Heroes");
            DropForeignKey("dbo.HeroFilms", "Film_Id", "dbo.Films");
            DropForeignKey("dbo.HeroFilms", new[] { "Hero_UniqNumber", "Hero_Name" }, "dbo.Heroes");
            DropForeignKey("dbo.HeroBattles", "Battle_Id", "dbo.battles");
            DropForeignKey("dbo.HeroBattles", new[] { "Hero_UniqNumber", "Hero_Name" }, "dbo.Heroes");
            DropForeignKey("dbo.HeroActors", "Actor_Id", "dbo.Actors");
            DropForeignKey("dbo.HeroActors", new[] { "Hero_UniqNumber", "Hero_Name" }, "dbo.Heroes");
            DropForeignKey("dbo.battles", "FilmId", "dbo.Films");
            DropForeignKey("dbo.FilmActors1", "Actor_Id", "dbo.Actors");
            DropForeignKey("dbo.FilmActors1", "Film_Id", "dbo.Films");
            DropForeignKey("dbo.FilmActors", "ActorId", "dbo.Actors");
            DropIndex("dbo.ComicsInfo", new[] { "Id" });
            DropIndex("dbo.HeroFilms", new[] { "Film_Id" });
            DropIndex("dbo.HeroFilms", new[] { "Hero_UniqNumber", "Hero_Name" });
            DropIndex("dbo.HeroBattles", new[] { "Battle_Id" });
            DropIndex("dbo.HeroBattles", new[] { "Hero_UniqNumber", "Hero_Name" });
            DropIndex("dbo.HeroActors", new[] { "Actor_Id" });
            DropIndex("dbo.HeroActors", new[] { "Hero_UniqNumber", "Hero_Name" });
            DropIndex("dbo.FilmActors1", new[] { "Actor_Id" });
            DropIndex("dbo.FilmActors1", new[] { "Film_Id" });
            DropIndex("dbo.Inventions", new[] { "Hero_UniqNumber", "Hero_Name" });
            DropIndex("dbo.battles", new[] { "FilmId" });
            DropIndex("dbo.FilmActors", new[] { "ActorId" });
            DropIndex("dbo.FilmActors", new[] { "FilmId" });
            DropTable("dbo.ComicsInfo");
            DropTable("dbo.HeroFilms");
            DropTable("dbo.HeroBattles");
            DropTable("dbo.HeroActors");
            DropTable("dbo.FilmActors1");
            DropTable("dbo.Comics");
            DropTable("dbo.Inventions");
            DropTable("dbo.Heroes");
            DropTable("dbo.battles");
            DropTable("dbo.Films");
            DropTable("dbo.FilmActors");
            DropTable("dbo.Actors");
        }
    }
}

﻿using Avengers.EF;
using Avengers.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Avengers
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly ApplicationContext db;
        public MainWindow()
        {
            InitializeComponent();
            db = new ApplicationContext();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {

        }


        private void Battles_Checked(object sender, RoutedEventArgs e) => grid1.ItemsSource = db.Battles.ToList();

        private void Actors_Checked(object sender, RoutedEventArgs e) => grid1.ItemsSource = db.Actors.ToList();


        private void ComicBooks_Checked(object sender, RoutedEventArgs e) => grid1.ItemsSource = db.ComicBooks.ToList();


        private void Films_Checked(object sender, RoutedEventArgs e) => grid1.ItemsSource = db.Films.ToList();



        private void Heroes_Checked(object sender, RoutedEventArgs e) => grid1.ItemsSource = db.Heroes.ToList();

        private void Inventions_Checked(object sender, RoutedEventArgs e) => grid1.ItemsSource = db.Inventions.ToList();

        private void Film_actor(object sender, RoutedEventArgs e)
        {
            try
            {
                if (grid1.SelectedItem == null)
                    throw new Exception("Do not select any objects");

                string newName = editTextBox.Text ?? throw new Exception("Input name");

                var entityType = grid1.SelectedItem.GetType();
                if (entityType.Name != "Film")
                    throw new Exception("You should choose film");

                dynamic entity = grid1.SelectedItem;
                int id = entity.Id;

                var result = db.Films.Where(p => p.Id == id).SelectMany(c => c.Actors);

                grid1.ItemsSource = result.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // фильмы и актеры в которых они снимались 
        private void Actor_Films(object sender, RoutedEventArgs e)
        {
            try
            {
                if (grid1.SelectedItem == null)
                    throw new Exception("Do not select any objects");

                string newName = editTextBox.Text ?? throw new Exception("Input name");

                var entityType = grid1.SelectedItem.GetType();
                if (entityType.Name != "Film")
                    throw new Exception("You should choose film");

                dynamic entity = grid1.SelectedItem;
                int id = entity.Id;

                var result = db.Actors.Where(p => p.Id == id).SelectMany(c => c.Films);

                grid1.ItemsSource = result.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (grid1.SelectedItem == null)
                    throw new Exception("Do not select any objects");

                string newName = editTextBox.Text ?? throw new Exception("Input name");

                var entityType = grid1.SelectedItem.GetType();
                dynamic entity = grid1.SelectedItem;
                int id = entity.Id;

                switch (entityType.Name.ToString())
                {
                    //Удаление с загрузкой данных
                    case "Actor":
                        Actor actor = db.Actors
                             .Where(c => c.Id == id)
                             .FirstOrDefault();
                        actor.Name = newName;

                        db.Entry(actor).State = EntityState.Modified;
                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;

                    case "Hero":
                        Hero hero = db.Heroes
                             .Where(c => c.UniqNumber == id)
                             .FirstOrDefault();

                        hero.Name = newName;
                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;

                        // Далее все удаления без загрузки данных



                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (grid1.SelectedItem == null)
                    throw new Exception("Do not select any objects");

                var entityType = grid1.SelectedItem.GetType();



                dynamic entity = grid1.SelectedItem;
                int id = entity.Id;
                switch (entityType.Name.ToString())
                {
                    //Удаление с загрузкой данных
                    case "Actor":
                        Actor actor = db.Actors
                            .Where(o => o.Id == id)
                            .FirstOrDefault();

                        db.Actors.Remove(actor);
                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;


                    case "Film":
                        Film film = db.Films
                             .Where(c => c.Id == id)
                             .FirstOrDefault();

                        db.Films.Remove(film);
                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;
                    case "Hero":
                        Hero hero = db.Heroes
                             .Where(c => c.UniqNumber == id)
                             .FirstOrDefault();

                        db.Heroes.Remove(hero);
                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;

                    // Далее все удаления без загрузки данных
                    case "Invention":
                        Invention invention = new Invention
                        {
                            Id = id
                        };

                        db.Inventions.Attach(invention);
                        db.Inventions.Remove(invention);

                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;
                    case "Battle":
                        Battle battle = new Battle
                        {
                            Id = id
                        };

                        db.Battles.Attach(battle);
                        db.Battles.Remove(battle);

                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;
                    case "ComicBook":
                        ComicBook comicBook = new ComicBook
                        {
                            Id = id
                        };

                        db.ComicBooks.Attach(comicBook);
                        db.ComicBooks.Remove(comicBook);

                        db.SaveChanges();
                        grid1.Items.Refresh();
                        break;


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void AddActorButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tb_name.Text ?? throw new Exception("Input name");
                string surname = tb_surname.Text ?? throw new Exception("Input surname");
                string salary = tb_salary.Text ?? throw new Exception("Input salary");

                Actor actor = new Actor
                {
                    Name = name,
                    Surname = surname,
                    Salary = int.Parse(salary),
                    Birthday = DateTime.Now,

                };

                db.Actors.Add(actor);
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //сколько актеров с каждой зарплатой
        private void groupBySalary_Click(object sender, RoutedEventArgs e)
        {

            var result = db.Actors.GroupBy(u => u.Salary).Select(g => new { Name = g.Key, Count = g.Count() }).ToList();
            grid1.ItemsSource = result;
        }

        //Вторая максимальная зп у актеров
        private void SecondMaxSalary_Click(object sender, RoutedEventArgs e)
        {
            var result = db.Actors.GroupBy(k => k.Salary)
                                        .OrderByDescending(g => g.Key)
                                        .Skip(1)
                                        .FirstOrDefault();
            grid1.ItemsSource = result.ToList();
        }

        //КОличество актеров в каждом фильме
        private void ActorsFilm_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                var result = db.Actors.SelectMany(c => c.Films).GroupBy(p => p.Title)
                        .Select(g => new
                        {
                            Name = g.Key,
                            Count = g.Count(),
                            Phones = g.Select(p => p)
                        });

                grid1.ItemsSource = result.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //актер с максимальной зп в определнном фильме
        private void highSalaryInTheFilm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (grid1.SelectedItem == null)
                    throw new Exception("Do not select any objects");

                string newName = editTextBox.Text ?? throw new Exception("Input name");

                var entityType = grid1.SelectedItem.GetType();
                if (entityType.Name != "Film")
                    throw new Exception("You should choose film");

                dynamic entity = grid1.SelectedItem;
                int id = entity.Id;

                var result = db.Films.Where(p => p.Id == id).SelectMany(c => c.Actors).ToList().Max(x => x.Salary);

                MessageBox.Show(result.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //имя начинается с
        private void NameStartFrom_Click(object sender, RoutedEventArgs e)
        {
            var result = db.Actors.Where(person => person.Name.StartsWith("N"))
                  .Take(50)
                  .GroupBy(person => person.Name);

            grid1.ItemsSource = result.ToList();
        }

        //средняя зп по актерам
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var result = db.Actors.Average(x => x.Salary);
            MessageBox.Show($"Averegae actor salary {result}");
        }

        //три таблицы
        private void b_Click(object sender, RoutedEventArgs e)
        {
            var result = db.Actors.SelectMany(c => c.Films).SelectMany(k => k.Battles).ToList();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            EFUtils.RollBack();
        }
    }
}

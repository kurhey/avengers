﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avengers.Models
{
    class Invention
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public int? HeroId { get; set; }
        public Hero Hero { get; set; }


    }
}

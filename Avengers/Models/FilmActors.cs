﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avengers.Models
{
    //Явное определение relation table для добавления дополнительного поля Rating
    class FilmActors
    {
        public int FilmId { get; set; }
        public Film Film { get; set; }
        public int ActorId { get; set; }
        public Actor Actor { get; set; }

        public decimal Rating { get; set; }


    }
}

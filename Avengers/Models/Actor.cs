﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avengers.Models
{
    class Actor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Birthday { get; set; }
        public int Salary { get; set; }


        public virtual ICollection<Hero> Heroes { get; set; }
        public virtual ICollection<Film> Films { get; set; }
        public virtual ICollection<FilmActors> FilmActors { get; set; }

        public Actor()
        {
            Heroes = new List<Hero>();
            Films = new List<Film>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avengers.Models
{
    class Hero
    {
        public int UniqNumber { get; set; }
        public string Name { get; set; }
        public string Side { get; set; }
        public bool Avenger { get; set; }
        public virtual ICollection<Film> Films { get; set; }
        public virtual ICollection<Actor> Actors { get; set; }
        public virtual ICollection<Invention> Inventions { get; set; }
        public virtual ICollection<Battle> Battles { get; set; }

        public Hero()
        {
            Actors = new List<Actor>();
            Films = new List<Film>();
            Inventions = new List<Invention>();
            Battles = new List<Battle>();
        }
    }
}

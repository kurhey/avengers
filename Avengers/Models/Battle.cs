﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Avengers.Models
{
    [Table("battles")]
    class Battle
    {
        public int Id { get; set; }
        public string Place { get; set; }

        public int? FilmId { get; set; }
        public Film Film { get; set; }

        public virtual ICollection<Hero> Heroes { get; set; }

        public Battle()
        {
            Heroes = new List<Hero>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avengers.Models
{
    class Film
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }

        public virtual ICollection<Actor> Actors { get; set; }
        public virtual ICollection<Hero> Heroes { get; set; }
        public virtual ICollection<Battle> Battles { get; set; }
        public virtual ICollection<FilmActors> FilmActors { get; set; }
        public Film()
        {
            Actors = new List<Actor>();
            Heroes = new List<Hero>();
            Battles = new List<Battle>();
        }

    }
}

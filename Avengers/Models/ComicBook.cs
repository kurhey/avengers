﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avengers.Models
{
    class ComicBook
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Part { get; set; }
        public string Publisher { get; set; }
        public int Rating { get; set; }
    }
}

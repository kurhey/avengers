﻿using Avengers.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Avengers.EF
{
    class ContextInitializer : DropCreateDatabaseIfModelChanges<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            Actor a = new Actor() { Name = "Mike", Surname = "Addams", Birthday = DateTime.Now };
            Actor a1 = new Actor() { Name = "Alex", Surname = "Test", Birthday = DateTime.Now };
            Actor a2 = new Actor() { Name = "Nike", Surname = "Test", Birthday = DateTime.Now };

            Film f1 = new Film() { Title = "Test1", Genre = "Action" };
            Film f2 = new Film() { Title = "Test2", Genre = "Action" };
            Film f3 = new Film() { Title = "Test3", Genre = "Action" };
            Film f4 = new Film() { Title = "Test4", Genre = "Action" };
            Film f5 = new Film() { Title = "Test5", Genre = "Action" };
            Film f6 = new Film() { Title = "Test6", Genre = "Action" };



            f1.Actors.Add(a);
            f1.Actors.Add(a1);
            f1.Actors.Add(a2);

            f2.Actors.Add(a);

            f3.Actors.Add(a1);
            f3.Actors.Add(a2);


            using (ApplicationContext db = new ApplicationContext())
            {
                db.Actors.AddRange(new List<Actor>() { a, a1, a2 });
                db.Films.AddRange(new List<Film>() { f1, f2, f3, f4, f5, f6 });
                db.SaveChanges();
            }
        }
    }
}

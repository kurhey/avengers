﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Avengers.Models;

namespace Avengers.EF
{
    class ApplicationContext : DbContext
    {
        static ApplicationContext()
        {
            Database.SetInitializer<ApplicationContext>(new ContextInitializer());
        }

        public ApplicationContext() : base("DBConnection")
        { }

        public DbSet<Actor> Actors { get; set; }
        public DbSet<ComicBook> ComicBooks  { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Hero> Heroes { get; set; }
        public DbSet<Invention> Inventions { get; set; }
        public DbSet<Battle> Battles { get; set; }
        public DbSet<FilmActors> FilmActors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //произвольные имена столбцов и произвольный Primary key
            modelBuilder.Entity<Hero>()
                .HasKey(key => new { key.UniqNumber, key.Name })
                .Property(item => item.Name).HasColumnName("HeroName");

            modelBuilder.Entity<FilmActors>().HasKey(q =>
               new
               {
                   q.FilmId,
                   q.ActorId
               });

            // Relationships 
            // Config relation table
            modelBuilder.Entity<FilmActors>()
                .HasRequired(t => t.Film)
                .WithMany(t => t.FilmActors)
                .HasForeignKey(t => t.FilmId);

            modelBuilder.Entity<FilmActors>()
                .HasRequired(t => t.Actor)
                .WithMany(t => t.FilmActors)
                .HasForeignKey(t => t.ActorId);


            //Одну модель в две таблицы
            modelBuilder.Entity<ComicBook>()
                .Map(option =>
                {
                    option.Properties(key => new { key.Title, key.Part });
                    option.ToTable("Comics");
                })
                .Map(option =>
                {
                    option.Properties(key => new { key.Publisher, key.Rating });
                    option.ToTable("ComicsInfo");
                });
            //для свойства Rating 2 цифры до и 2 после запятой
            modelBuilder.Entity<FilmActors>().Property(p => p.Rating).HasPrecision(2, 2);


            
            modelBuilder.Entity<Actor>().Property(p => p.Name).HasColumnType("varchar");
            modelBuilder.Entity<Actor>().Property(p => p.Salary).HasColumnType("int");
            modelBuilder.Entity<Actor>().Property(p => p.Birthday).HasColumnType("date");

        }

    }
}

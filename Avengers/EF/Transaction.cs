﻿using Avengers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Avengers.EF
{
    class Transaction
    {
        public static void CreateTrans()
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        Actor p1 = context.Actors.Find(5);
                        Actor p2 = context.Actors.Find(6);
                        int delta = 3000;
                        p1.Salary = p1.Salary + delta;
                        if (context.ChangeTracker.HasChanges() == true)
                        {
                            p2.Salary = p2.Salary - delta;
                            if (p2.Salary < 0)
                            {
                                transaction.Rollback();
                            }
                            else
                            {
                                context.SaveChanges();
                                transaction.Commit();
                            }
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        MessageBox.Show(ex.Message);
                    }


                }
            }
        }
    }
}
